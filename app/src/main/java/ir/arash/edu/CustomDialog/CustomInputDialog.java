package ir.arash.edu.CustomDialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CustomInputDialog {

    private AlertDialog InputDialog;
    private View view;
    private TextView DialogTitle;
    private TextView DialogDesc;
    private EditText DialogInput;
    private TextView btnOK;
    private TextView btnCancel;

    private void initViews(View view) {
        DialogTitle = view.findViewById(R.id.DialogTitle);
        DialogDesc = view.findViewById(R.id.DialogDesc);
        DialogInput = view.findViewById(R.id.DialogInput);
        btnOK = view.findViewById(R.id.btnOK);
        btnCancel = view.findViewById(R.id.btnCancel);
    }

    public interface OnClickListener{
        void onClick(AlertDialog alertDialog, CharSequence Dialog_input);
    }

    public CustomInputDialog(Context context, CharSequence title, CharSequence Description, CharSequence inputHint) {

        InputDialog = new AlertDialog.Builder(context).create();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.inputdialog_style, null);
        initViews(view);
        DialogTitle.setText(title);
        DialogDesc.setText(Description);
        DialogInput.setHint(inputHint);
        InputDialog.setView(view);
    }

    public void show() {
        if (InputDialog != null && !InputDialog.isShowing()) {
            InputDialog.show();
        }
    }

    public void Cancelable(boolean cancel) {
        InputDialog.setCancelable(cancel);
    }

    public void setPositiveBtn(CharSequence text, final OnClickListener onClickListener) {
        if (text != null) {
            btnOK.setVisibility(View.VISIBLE);
            btnOK.setText(text);

            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onClickListener != null){
                        onClickListener.onClick(InputDialog,DialogInput.getText().toString());
                        return;
                    }
                }
            });

            return;
        }
        btnOK.setVisibility(View.GONE);
    }
    public void setNegativeBtn(CharSequence text, final OnClickListener onClickListener) {
        if (text != null) {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(text);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onClickListener != null){
                        onClickListener.onClick(InputDialog,DialogInput.getText().toString());
                    }
                }
            });

            return;
        }
        btnCancel.setVisibility(View.GONE);
    }
}