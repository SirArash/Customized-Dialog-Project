package ir.arash.edu.CustomDialog;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnShowCustom;
    private TextView Returnedvalue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnShowCustom = findViewById(R.id.btnShowCustom);
        Returnedvalue = findViewById(R.id.Returnedvalue);

        btnShowCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomInputDialog editText = new CustomInputDialog(MainActivity.this
                ,"Hello","PLZ enter yr name : ", "Type Your Name Here");
                editText.setPositiveBtn("ok", new CustomInputDialog.OnClickListener() {
                    @Override
                    public void onClick(AlertDialog alertDialog, CharSequence Dialog_input) {
                        Returnedvalue.setText(Dialog_input);
                       alertDialog.dismiss();
                    }
                });
                editText.setNegativeBtn("cancel", new CustomInputDialog.OnClickListener() {
                    @Override
                    public void onClick(AlertDialog alertDialog, CharSequence Dialog_input) {
                        Toast.makeText(MainActivity.this, "Operation Cancelled!!", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }
                });
                editText.show();
                editText.Cancelable(false);
            }
        });

    }
}